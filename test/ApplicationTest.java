import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import models.TodoList;

import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.data.DynamicForm;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.RequiredValidator;
import play.i18n.Lang;
import play.libs.F;
import play.libs.F.*;
import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

public class ApplicationTest extends BaseApplicationTest {
    @Test
    public void index() {
        Result result = route(fakeRequest(GET, "/"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("TODO: Play!");
    }

    @Test
    public void login() {
        Result result = route(fakeRequest(GET, "/login"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("Login");
    }

    @Test
    public void authenticationFailed() {
        Map<String, String> body = new HashMap<>();
        body.put("email","me@email.com");
        body.put("password","wrong");
        Result result = route(csrfRequest(POST, "/login").withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("Invalid user or password");
        assertThat(session(result).get("email")).isNull();
    }

    @Test
    public void authenticationSucceeded() {
        Map<String, String> body = new HashMap<>();
        body.put("email","me@email.com");
        body.put("password","password");
        Result result = route(csrfRequest(POST, "/login").withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        assertThat(redirectLocation(result)).isEqualTo("/lists");
        assertThat(session(result).get("email")).isEqualTo("me@email.com");
    }

    @Test
    public void authenticationRequired() {
        Result result = route(fakeRequest(GET, "/lists"));
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        assertThat(redirectLocation(result)).isEqualTo("/login");
    }

    @Test
    public void authenticatedAccess() {
        Result result = route(authenticatedRequest(GET, "/lists"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("To Do Lists");
    }
}
