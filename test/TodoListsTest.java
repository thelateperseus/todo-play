import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;

import models.TodoList;

import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.data.DynamicForm;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.RequiredValidator;
import play.db.jpa.JPA;
import play.i18n.Lang;
import play.libs.F;
import play.libs.F.*;
import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;


public class TodoListsTest extends BaseApplicationTest {
    @Test
    public void viewLists() {
        Result result = route(authenticatedRequest(GET, "/lists"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("To Do Lists");
    }
    
    @Test
    public void viewList() {
        Result result = route(authenticatedRequest(GET, "/lists/2"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("Groceries");
    }

    @Test
    public void addListForm() {
        Result result = route(authenticatedRequest(GET, "/lists/add"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("Add New List");
    }

    @Test
    public void addList() {
        Map<String, String> body = new HashMap<>();
        body.put("name","New List");
        Result result = route(
            authenticatedRequest(POST, "/lists")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        Matcher matcher = 
            Pattern.compile("/lists/(\\d+)").matcher(redirectLocation(result));
        assertThat(matcher.matches()).isTrue();
        TodoList list = em().find(TodoList.class, new Long(matcher.group(1)));
        assertThat(list).isNotNull();
        assertThat(list.getName()).isEqualTo("New List");
    }

    @Test
    public void addListInvalid() {
        Map<String, String> body = new HashMap<>();
        Result result = route(
            authenticatedRequest(POST, "/lists")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("This field is required");
    }
}
