import static play.test.Helpers.fakeRequest;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.filters.csrf.CSRF;
import play.filters.csrf.CSRFFilter;
import play.test.FakeApplication;
import play.test.FakeRequest;
import play.test.Helpers;
import scala.Option;

public class BaseApplicationTest {

    public static FakeApplication app;

    @BeforeClass
    public static void startApp() {
        Map<String, String> params = new HashMap<>( Helpers.inMemoryDatabase() );
        params.put("evolutionplugin", "disabled");
        params.put("jpa.default", "test");
        app = Helpers.fakeApplication(params);
        Helpers.start(app);
    }

    @AfterClass
    public static void stopApp() {
        Helpers.stop(app);
    }

    protected static EntityManager em() {
        return JPA.em("default");
    }
    
    protected static FakeRequest csrfRequest(String method, String url) {
        String token = CSRFFilter.apply$default$5().generateToken();
        return fakeRequest(method, url + "?csrfToken=" + token)
            .withSession(CSRF.TokenName(), token);
    }

    protected static FakeRequest authenticatedRequest(String method, String url) {
        return csrfRequest(method, url).withSession("email", "me@email.com");
    }
}