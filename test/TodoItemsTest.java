import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import models.TodoItem;
import models.TodoList;

import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.data.DynamicForm;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.RequiredValidator;
import play.db.jpa.JPA;
import play.i18n.Lang;
import play.libs.F;
import play.libs.F.*;
import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;


public class TodoItemsTest extends BaseApplicationTest {
    @Test
    public void addItemForm() {
        Result result = route(authenticatedRequest(GET, "/lists/2/items/add"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("Add New Item to List");
    }

    @Test
    public void addItem() {
        Map<String, String> body = new HashMap<>();
        body.put("text","New Item");
        Result result = route(
            authenticatedRequest(POST, "/lists/2/items")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        assertThat(redirectLocation(result)).isEqualTo("/lists/2");
        TodoItem item = em().find(TodoItem.class, 7L);
        assertThat(item).isNotNull();
        assertThat(item.getText()).isEqualTo("New Item");
    }

    @Test
    public void addItemInvalid() {
        Map<String, String> body = new HashMap<>();
        Result result = route(
            authenticatedRequest(POST, "/lists/2/items")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("This field is required");
    }

    @Test
    public void editItemForm() {
        Result result = route(authenticatedRequest(GET, "/lists/2/items/4"));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("Edit Item in List");
    }

    @Test
    public void editItem() {
        Map<String, String> body = new HashMap<>();
        body.put("text","New Text");
        Result result = route(
            authenticatedRequest(POST, "/lists/2/items/5")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        assertThat(redirectLocation(result)).isEqualTo("/lists/2");
        TodoItem item = em().find(TodoItem.class, 5L);
        assertThat(item).isNotNull();
        assertThat(item.getText()).isEqualTo("New Text");
        assertThat(item.isDone()).isTrue();
    }

    @Test
    public void editItemInvalid() {
        Map<String, String> body = new HashMap<>();
        Result result = route(
            authenticatedRequest(POST, "/lists/2/items/4")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(OK);
        assertThat(contentAsString(result)).contains("This field is required");
    }

    @Test
    public void updateDone() {
        Map<String, String> body = new HashMap<>();
        body.put("id","4");
        body.put("done","true");
        Result result = route(
            authenticatedRequest(POST, "/lists/2/items/updateDone")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(OK);
        TodoItem item = em().find(TodoItem.class, 4L);
        assertThat(item).isNotNull();
        assertThat(item.isDone()).isTrue();
    }

    @Test
    public void deleteItem() {
        Map<String, String> body = new HashMap<>();
        body.put("id","6");
        Result result = route(
            authenticatedRequest(POST, "/lists/3/items/deleteItem")
                .withFormUrlEncodedBody(body));
        assertThat(status(result)).isEqualTo(OK);
        TodoItem item = em().find(TodoItem.class, 6L);
        assertThat(item).isNull();
    }
}
