import java.lang.reflect.Method;

import play.Application;
import play.GlobalSettings;
import play.api.mvc.EssentialFilter;
import play.filters.csrf.CSRFFilter;
import play.mvc.Action;
import play.mvc.Http.Request;

import controllers.CurrentUser;
import controllers.InitialData;


public class Global extends GlobalSettings {
    @Override
    public Action onRequest(Request request, Method actionMethod) {
        return new CurrentUser();
    }
    
    @Override
    public void onStart(Application app) {
        InitialData.insert(app);
    }

    @Override
    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{CSRFFilter.class};
    }
}
