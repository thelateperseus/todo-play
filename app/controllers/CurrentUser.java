package controllers;
import models.AppUser;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Http.Context;


public final class CurrentUser extends Action.Simple {
    
    public Promise<Result> call(Context ctx) throws Throwable {
        ctx.args.put("user", AppUser.findByEmail(ctx.session().get("email")));
        return delegate.call(ctx);
    }
    
    public static AppUser current() {
        return (AppUser) Http.Context.current().args.get("user");
    }
}