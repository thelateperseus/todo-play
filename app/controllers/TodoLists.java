package controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import models.TodoItem;
import models.TodoList;
import play.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

@Transactional
@Security.Authenticated(Secured.class)
public class TodoLists extends Controller {

    public static Result addListForm() {
        return ok(addListForm.render(Form.form(TodoList.class)));
    }

    public static Result addList() {
        Form<TodoList> form = Form.form(TodoList.class).bindFromRequest();
        if ( form.hasErrors() ) {
            return ok(addListForm.render(form));
        } else {
            TodoList list = form.get();
            list.setOwner(CurrentUser.current());
            JPA.em().persist(list);
            return redirect(routes.TodoLists.viewList(list.getId()));
        }
    }

    public static Result viewLists() {
        List<TodoList> lists = TodoList.findAllByUser(CurrentUser.current());
        return ok(viewLists.render(lists));
    }

    public static Result viewList(Long id) {
        Form<TodoItem> form = Form.form(TodoItem.class);
        TodoList list = TodoList.findList(CurrentUser.current(), id);
        return ok(viewList.render(list,form));
    }

}
