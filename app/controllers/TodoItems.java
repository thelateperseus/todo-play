package controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import models.TodoItem;
import models.TodoList;
import play.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

@Transactional
@Security.Authenticated(Secured.class)
public class TodoItems extends Controller {

    public static Result addItemForm(Long listId) {
        TodoList list = TodoList.findList(CurrentUser.current(), listId);
        return ok(addItemForm.render(Form.form(TodoItem.class), list, null));
    }

    public static Result addItem(Long listId) {
        Form<TodoItem> form = Form.form(TodoItem.class).bindFromRequest();
        TodoList list = TodoList.findList(CurrentUser.current(), listId);
        if ( form.hasErrors() ) {
            return ok(addItemForm.render(form, list, null));
        } else {
            TodoItem item = form.get();
            item.setList(list);
            JPA.em().persist(item);
            return redirect(routes.TodoLists.viewList(listId));
        }
    }

    public static Result editItemForm(Long listId, Long itemId) {
        TodoItem item = TodoItem.findItem(CurrentUser.current(), listId, itemId);
        TodoList list = item.getList();
        Form<TodoItem> form = Form.form(TodoItem.class).fill(item);
        return ok(addItemForm.render(form, list, item));
    }

    public static Result editItem(Long listId, Long itemId) {
        TodoItem item = TodoItem.findItem(CurrentUser.current(), listId, itemId);
        TodoList list = item.getList();
        Form<TodoItem> form = Form.form(TodoItem.class).bindFromRequest();
        if ( form.hasErrors() ) {
            return ok(addItemForm.render(form, list, item));
        } else {
            TodoItem updatedItem = form.get();
            updatedItem.setList(list);
            updatedItem.setDone(item.isDone());
            updatedItem.setId(itemId);
            JPA.em().merge(updatedItem);
            return redirect(routes.TodoLists.viewList(listId));
        }
    }

    public static Result updateDone(Long listId) {
        Map<String, String[]> postData = request().body().asFormUrlEncoded();
        Long id = new Long( postData.get( "id" )[0] );
        boolean done = "true".equals( postData.get( "done" )[0] );
        TodoItem item = TodoItem.findItem(CurrentUser.current(), listId, id);
        item.setDone(done);
        return ok();
    }

    public static Result deleteItem(Long listId) {
        Map<String, String[]> postData = request().body().asFormUrlEncoded();
        Long id = new Long( postData.get( "id" )[0] );
        TodoItem item = TodoItem.findItem(CurrentUser.current(), listId, id);
        JPA.em().remove(item);
        return ok();
    }

}
