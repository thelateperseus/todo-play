$(document).ready(function() {
	"use strict";
	
	$.get('/lists', function(data) {
		$.each(data, function(index, list) {
			$('#lists').append($('<li>').text(list.name));
		});
	});
});